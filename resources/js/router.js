import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store/index.js';
import Login from '../layouts/Login.vue';

import POS from '../layouts/after_login/POS/Index.vue';
import Home from '../layouts/after_login/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    component: Login,
    name: 'Login',
    meta: { requiresAuth: false }
  },
  {
    path: '/',
    component: Home,
    meta: { requiresAuth: true },
    redirect: '/pos',
    children: [
      {
        path: '/pos',
        name: 'POS',
        component: POS,
        meta: {
          requiresAuth: true,
          name: 'POS'
        }
      },
    ]
  }
];

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'is-active',
  routes
});

router.beforeEach(async (to, from, next) => {
  if (
    !to.matched.some(record => record.meta.requiresAuth) &&
        store.getters.isLoggedIn
  ) {
    await store.dispatch('getCurrentUser');
    next('/');
  } else if (
    to.matched.some(record => record.meta.requiresAuth) &&
        !store.getters.isLoggedIn
  ) {
    alert('You need to login first');
    next('/login');
  } else if (store.getters.isLoggedIn) {
    await store.dispatch('getCurrentUser').catch(() => {
      next('/login');
    });
    if (to.meta.middleware) {
      if (!store.getters.currentUser.user.role.activePermissions.some(e => e.name === to.meta.middleware.auth)) {
        alert('You didn\'t have permission');
        next(from.path);
      }
    }
    next();
  } else {
    next();
  }
});

export default router;
