import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth/auth';
import user from './UserManagement/user/user';
import role from './UserManagement/role/role';
import product from './ProductManagement/product/product';
import category from './ProductManagement/category/category';
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth: auth,
    user:user,
    role:role,
    product: product,
    category:category,
  }
});
