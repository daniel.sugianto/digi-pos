import ApiService from '@/common/api.service';
const endpoint = '/categories';
const state = {};

const getters = {};

const actions = {
  getAllCategoryIndex (state, params) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '?' + params).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  storeCategory (state, request) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.post(endpoint, request).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  updateCategory (state, request) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.update(endpoint, request.id, request).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  deleteCategory (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.delete(endpoint + '/' + id).then(
        response => {
          resolve(response.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getEditCategoryData (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '/' + id + '/edit').then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
};

const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters
};
