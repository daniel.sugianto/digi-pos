import ApiService from '@/common/api.service';
const endpoint = '/products';
const state = {};

const getters = {};

const actions = {
  getAllProductIndex (state, params) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '?' + params).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  storeProduct (state, request) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.setHeaderMultipartFormData();
      ApiService.postFormData(endpoint, request.product).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  updateProduct (state, request) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.setHeaderMultipartFormData();
      ApiService.updateFormData(endpoint, request.id, request.product).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  deleteProduct (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.delete(endpoint + '/' + id).then(
        response => {
          resolve(response.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getEditProductData (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '/' + id + '/edit').then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getCreateProductData (state) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '/create').then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  }
};

const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters
};
