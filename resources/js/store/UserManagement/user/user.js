import ApiService from '@/common/api.service';
const endpoint = '/users';
const state = {};

const getters = {};

const actions = {
  getAllUserIndex (state, params) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '?' + params).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  storeUser (state, request) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.setHeaderMultipartFormData();
      ApiService.postFormData(endpoint, request.user).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  updateUser (state, request) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.setHeaderMultipartFormData();
      ApiService.updateFormData(endpoint, request.id, request.user).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  deleteUser (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.delete(endpoint + '/' + id).then(
        response => {
          resolve(response.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getEditUserData (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '/' + id + '/edit').then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getCreateUserData (state) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '/create').then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  }
};

const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters
};
