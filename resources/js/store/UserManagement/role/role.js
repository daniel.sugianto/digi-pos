import ApiService from '@/common/api.service';
const endpoint = '/roles';
const state = {};

const getters = {};

const actions = {
  getAllRoleIndex (state, params) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '?' + params).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  storeRole (state, rolePermissions) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.post(endpoint, rolePermissions).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  updateRole (state, rolePermissions) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.update(
        endpoint, rolePermissions.role.id,
        rolePermissions
      ).then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  deleteRole (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.delete(endpoint + '/' + id).then(
        response => {
          resolve(response.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getEditRoleData (state, id) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '/' + id + '/edit').then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getCreateRoleData (state) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get(endpoint + '/create').then(
        response => {
          resolve(response.data.data);
        },
        error => {
          reject(error);
        }
      );
    });
  }
};

const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters
};
