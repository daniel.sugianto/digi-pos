const ID_TOKEN_KEY =  process.env.MIX_APP_TOKEN_NAME;

export const getToken = () => {
  return localStorage.getItem(ID_TOKEN_KEY);
};

export const saveToken = token => {
  localStorage.setItem(ID_TOKEN_KEY, token);
};

export const destroyToken = () => {
  localStorage.clear();
};

export default { getToken, saveToken, destroyToken };
