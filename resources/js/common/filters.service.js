import Vue from 'vue';
import moment from 'moment';
Vue.filter('toCurrency', function (value) {
  const parsedValue = parseInt(value);
  if (isNaN(parsedValue)) {
    return value;
  }
  const formatter = new Intl.NumberFormat('id', {
    style: 'currency',
    currency: 'IDR',
    minimumFractionDigits: 2
  });
  return formatter.format(parsedValue);
});

Vue.filter('toCommas', function (value) {
  if (typeof value !== 'number') {
    return value;
  }
  const formatter = new Intl.NumberFormat('id');
  return formatter.format(value);
});

Vue.filter('dateFormat', function (date, format) {
  return moment
    .utc(date)
    .local()
    .format(format);
});
